﻿using CRUDAlunosT1.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUDAlunosT1
{
    public partial class Form1 : Form
    {
        List<Aluno> ListaDeAlunos = new List<Aluno>();

        Aluno alunoEncontrado = null;

        public Form1()
        {
            InitializeComponent();

            ListaDeAlunos.Add(new Aluno {IdAluno = 1, PrimeiroNome = "Afonso", Apelido = "Monteiro"});
            ListaDeAlunos.Add(new Aluno { IdAluno = 2, PrimeiroNome = "André", Apelido = "Gomes" });
            ListaDeAlunos.Add(new Aluno { IdAluno =3, PrimeiroNome = "André", Apelido = "Nunes"});
            ListaDeAlunos.Add(new Aluno { IdAluno = 4, PrimeiroNome = "Andrii", Apelido = "Atamanchuk"});
            ListaDeAlunos.Add(new Aluno { IdAluno = 5, PrimeiroNome = "Bruno", Apelido = "Teixeira"});
            ListaDeAlunos.Add(new Aluno { IdAluno = 6, PrimeiroNome = "Denilson", Apelido = "Santana"});
            ListaDeAlunos.Add(new Aluno { IdAluno = 7, PrimeiroNome = "Diogo", Apelido = "Martins"});
            ListaDeAlunos.Add(new Aluno { IdAluno = 8, PrimeiroNome = "Dorival", Apelido = "Gonga" });
            ListaDeAlunos.Add(new Aluno { IdAluno = 9, PrimeiroNome = "Edgar", Apelido = "Cardoso" });
            ListaDeAlunos.Add(new Aluno { IdAluno = 10, PrimeiroNome = "Fábio", Apelido = "Barradas" });

            LoadListas();
 
        }

        private void LoadListas()
        {
            ComboBoxTodosAlunos.DataSource = ListaDeAlunos;
            //ComboBoxTodosAlunos.DisplayMember = "NomeCompleto";

            ListBoxTodosAlunos.DataSource = ListaDeAlunos;
            ListBoxTodosAlunos.DisplayMember = "NomeCompleto";
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ComboBoxProcurar.SelectedIndex == -1)
            {
                MessageBox.Show(this, "Escolha um critério", "Cabeçudo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (ComboBoxProcurar.SelectedIndex == 0)
            {
                if (string.IsNullOrEmpty(TextBoxProcurar.Text))
                {
                    MessageBox.Show("Insira um Id de aluno!");
                    return;
                }

                int idaluno;
                if (!int.TryParse(TextBoxProcurar.Text, out idaluno))
                {
                    MessageBox.Show("Tem que inserir um número");
                    return;
                }

                var aluno = new Aluno
                {
                    IdAluno = idaluno,
                };

                alunoEncontrado = ProcurarAluno(aluno);

                if (alunoEncontrado != null)
                {
                    modoEditar();

                    TextBoxIdAluno.Text = alunoEncontrado.IdAluno.ToString();
                    TextBoxPrimeiroNome.Text = alunoEncontrado.PrimeiroNome;
                    TextBoxApelido.Text = alunoEncontrado.Apelido;
                }
                else
                {
                    MessageBox.Show("Esse aluno não existe!");
                    return;
                }
            }
            else
            {
                MessageBox.Show("Escolheu procurar por Apelido!");
            }
        }


        private Aluno ProcurarAluno(Aluno aluno)
        {
            foreach(Aluno a in ListaDeAlunos)
            {
                if(aluno.IdAluno == a.IdAluno)
                {
                    return a;
                }
            }

            return null;

        }

        private void modoEditar()
        {
            TextBoxIdAluno.Enabled = true;
            TextBoxPrimeiroNome.Enabled = true;
            TextBoxApelido.Enabled = true;
            ButtonUpdate.Enabled = true;
            ButtonCancelar.Enabled = true;
        }

        private void ButtonCancelar_Click(object sender, EventArgs e)
        {
            TextBoxIdAluno.Text = string.Empty;
            TextBoxPrimeiroNome.Text = string.Empty;
            TextBoxApelido.Text = string.Empty;

            ButtonCancelar.Enabled = false;
            ButtonUpdate.Enabled = false;

            TextBoxProcurar.Text = string.Empty;
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxPrimeiroNome.Text))
            {
                MessageBox.Show("Escreve o primeiro nome.");
                return;
            }

            if (string.IsNullOrEmpty(TextBoxApelido.Text))
            {
                MessageBox.Show("Escreva o apelido.");
                return;
            }

            alunoEncontrado.PrimeiroNome = TextBoxPrimeiroNome.Text;
            alunoEncontrado.Apelido = TextBoxApelido.Text;

            MessageBox.Show("Aluno atualizado com sucesso!");

            atualizaListas();

            TextBoxIdAluno.Text = string.Empty;
            TextBoxPrimeiroNome.Text = string.Empty;
            TextBoxApelido.Text = string.Empty;

            ButtonCancelar.Enabled = false;
            ButtonUpdate.Enabled = false;

            TextBoxProcurar.Text = string.Empty;
        }

        private void atualizaListas()
        {
            ComboBoxTodosAlunos.DataSource = null;
            ComboBoxTodosAlunos.DataSource = ListaDeAlunos;

            ListBoxTodosAlunos.DataSource = null;
            ListBoxTodosAlunos.DataSource = ListaDeAlunos;
        }
    }
}
