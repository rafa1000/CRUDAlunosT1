﻿namespace CRUDAlunosT1
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ListBoxTodosAlunos = new System.Windows.Forms.ListBox();
            this.ComboBoxTodosAlunos = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ButtonProcurar = new System.Windows.Forms.Button();
            this.ComboBoxProcurar = new System.Windows.Forms.ComboBox();
            this.TextBoxProcurar = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxIdAluno = new System.Windows.Forms.TextBox();
            this.TextBoxPrimeiroNome = new System.Windows.Forms.TextBox();
            this.TextBoxApelido = new System.Windows.Forms.TextBox();
            this.ButtonUpdate = new System.Windows.Forms.Button();
            this.ButtonCancelar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ListBoxTodosAlunos);
            this.groupBox1.Controls.Add(this.ComboBoxTodosAlunos);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(39, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(330, 195);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Listagem de Alunos";
            // 
            // ListBoxTodosAlunos
            // 
            this.ListBoxTodosAlunos.FormattingEnabled = true;
            this.ListBoxTodosAlunos.ItemHeight = 16;
            this.ListBoxTodosAlunos.Location = new System.Drawing.Point(15, 75);
            this.ListBoxTodosAlunos.Name = "ListBoxTodosAlunos";
            this.ListBoxTodosAlunos.Size = new System.Drawing.Size(257, 116);
            this.ListBoxTodosAlunos.TabIndex = 1;
            // 
            // ComboBoxTodosAlunos
            // 
            this.ComboBoxTodosAlunos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxTodosAlunos.FormattingEnabled = true;
            this.ComboBoxTodosAlunos.Location = new System.Drawing.Point(14, 32);
            this.ComboBoxTodosAlunos.Name = "ComboBoxTodosAlunos";
            this.ComboBoxTodosAlunos.Size = new System.Drawing.Size(303, 24);
            this.ComboBoxTodosAlunos.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TextBoxProcurar);
            this.groupBox2.Controls.Add(this.ButtonProcurar);
            this.groupBox2.Controls.Add(this.ComboBoxProcurar);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(428, 37);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(322, 180);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Procurar";
            // 
            // ButtonProcurar
            // 
            this.ButtonProcurar.Image = ((System.Drawing.Image)(resources.GetObject("ButtonProcurar.Image")));
            this.ButtonProcurar.Location = new System.Drawing.Point(241, 110);
            this.ButtonProcurar.Name = "ButtonProcurar";
            this.ButtonProcurar.Size = new System.Drawing.Size(62, 53);
            this.ButtonProcurar.TabIndex = 1;
            this.ButtonProcurar.UseVisualStyleBackColor = true;
            this.ButtonProcurar.Click += new System.EventHandler(this.button1_Click);
            // 
            // ComboBoxProcurar
            // 
            this.ComboBoxProcurar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxProcurar.FormattingEnabled = true;
            this.ComboBoxProcurar.Items.AddRange(new object[] {
            "Id Aluno",
            "Apelido"});
            this.ComboBoxProcurar.Location = new System.Drawing.Point(19, 21);
            this.ComboBoxProcurar.Name = "ComboBoxProcurar";
            this.ComboBoxProcurar.Size = new System.Drawing.Size(121, 24);
            this.ComboBoxProcurar.TabIndex = 0;
            // 
            // TextBoxProcurar
            // 
            this.TextBoxProcurar.Location = new System.Drawing.Point(156, 25);
            this.TextBoxProcurar.Name = "TextBoxProcurar";
            this.TextBoxProcurar.Size = new System.Drawing.Size(147, 22);
            this.TextBoxProcurar.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ButtonCancelar);
            this.groupBox3.Controls.Add(this.ButtonUpdate);
            this.groupBox3.Controls.Add(this.TextBoxApelido);
            this.groupBox3.Controls.Add(this.TextBoxPrimeiroNome);
            this.groupBox3.Controls.Add(this.TextBoxIdAluno);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(39, 227);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(711, 102);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Editar";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID de Aluno : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(227, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Primeiro Nome : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(485, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Apelido : ";
            // 
            // TextBoxIdAluno
            // 
            this.TextBoxIdAluno.Enabled = false;
            this.TextBoxIdAluno.Location = new System.Drawing.Point(112, 25);
            this.TextBoxIdAluno.Name = "TextBoxIdAluno";
            this.TextBoxIdAluno.ReadOnly = true;
            this.TextBoxIdAluno.Size = new System.Drawing.Size(95, 22);
            this.TextBoxIdAluno.TabIndex = 3;
            // 
            // TextBoxPrimeiroNome
            // 
            this.TextBoxPrimeiroNome.Enabled = false;
            this.TextBoxPrimeiroNome.Location = new System.Drawing.Point(351, 26);
            this.TextBoxPrimeiroNome.Name = "TextBoxPrimeiroNome";
            this.TextBoxPrimeiroNome.Size = new System.Drawing.Size(115, 22);
            this.TextBoxPrimeiroNome.TabIndex = 4;
            // 
            // TextBoxApelido
            // 
            this.TextBoxApelido.Enabled = false;
            this.TextBoxApelido.Location = new System.Drawing.Point(562, 26);
            this.TextBoxApelido.Name = "TextBoxApelido";
            this.TextBoxApelido.Size = new System.Drawing.Size(130, 22);
            this.TextBoxApelido.TabIndex = 5;
            // 
            // ButtonUpdate
            // 
            this.ButtonUpdate.Enabled = false;
            this.ButtonUpdate.Image = ((System.Drawing.Image)(resources.GetObject("ButtonUpdate.Image")));
            this.ButtonUpdate.Location = new System.Drawing.Point(564, 54);
            this.ButtonUpdate.Name = "ButtonUpdate";
            this.ButtonUpdate.Size = new System.Drawing.Size(62, 42);
            this.ButtonUpdate.TabIndex = 6;
            this.ButtonUpdate.UseVisualStyleBackColor = true;
            this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // ButtonCancelar
            // 
            this.ButtonCancelar.Enabled = false;
            this.ButtonCancelar.Image = ((System.Drawing.Image)(resources.GetObject("ButtonCancelar.Image")));
            this.ButtonCancelar.Location = new System.Drawing.Point(630, 54);
            this.ButtonCancelar.Name = "ButtonCancelar";
            this.ButtonCancelar.Size = new System.Drawing.Size(62, 42);
            this.ButtonCancelar.TabIndex = 7;
            this.ButtonCancelar.UseVisualStyleBackColor = true;
            this.ButtonCancelar.Click += new System.EventHandler(this.ButtonCancelar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 352);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "CRUD de Alunos";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ComboBoxTodosAlunos;
        private System.Windows.Forms.ListBox ListBoxTodosAlunos;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ComboBoxProcurar;
        private System.Windows.Forms.Button ButtonProcurar;
        private System.Windows.Forms.TextBox TextBoxProcurar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button ButtonCancelar;
        private System.Windows.Forms.Button ButtonUpdate;
        private System.Windows.Forms.TextBox TextBoxApelido;
        private System.Windows.Forms.TextBox TextBoxPrimeiroNome;
        private System.Windows.Forms.TextBox TextBoxIdAluno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}

