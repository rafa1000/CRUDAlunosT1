﻿namespace CRUDAlunosT1.Modelos
{
    public class Aluno
    {
        public int IdAluno { get; set; }

        public string PrimeiroNome { get; set; }

        public string Apelido { get; set; }

        public string NomeCompleto
        {
            get
            {
                return $"{PrimeiroNome} {Apelido}";
            }
        }

        public override string ToString()
        {
            return $"{IdAluno} - {PrimeiroNome} {Apelido}";
        }

    }
}
